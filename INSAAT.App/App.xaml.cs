﻿using log4net;
using log4net.Config;
using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

[assembly: log4net.Config.XmlConfigurator(Watch=true)]
namespace INSAAT.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e)
        {
            BasicConfigurator.Configure();

            //logger.Debug("Here is a debug log.");
            //logger.Info("... and an Info log.");
            //logger.Warn("... and a warning.");
            //logger.Error("... and an error.");
            //logger.Fatal("... and a fatal error.");

            //// get the theme from the current application
            //var theme = ThemeManager.DetectAppStyle(Application.Current);

            //// now set the Green accent and dark theme
            //ThemeManager.ChangeAppStyle(Application.Current,
            //                            ThemeManager.GetAccent("Crimson"),
            //                            ThemeManager.GetAppTheme("BaseLight"));

            base.OnStartup(e);
        }
    }
}
