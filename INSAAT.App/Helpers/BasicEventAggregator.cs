﻿using System;
using System.Collections.Generic;

namespace INSAAT.App.Helpers
{
    public static class BasicEventAggregator 
    {
        private static readonly Dictionary<string, List<EventHandlerBase>> subscriptions;

        static BasicEventAggregator()
        {
            subscriptions = new Dictionary<string, List<EventHandlerBase>>();
        }

        public static void Subscribe(string eventName, EventHandlerBase handler)
        {
            List<EventHandlerBase> handlers;
            if (!subscriptions.TryGetValue(eventName, out handlers))
            {
                handlers = new List<EventHandlerBase>();
                subscriptions.Add(eventName, handlers);
            }
            handlers.Add(handler);
        }

        public static void Publish<TPayload>(string eventName, TPayload payload)
        {
            if (subscriptions.ContainsKey(eventName))
            {
                foreach (EventHandlerBase handler in subscriptions[eventName])
                {
                    handler.InvokeCode(payload);
                }
            }
        }
    }

    public abstract class EventHandlerBase
    {
        public abstract void InvokeCode(object parameter);
    }

    public class AggregatedEventHandler<T> : EventHandlerBase
    {
        public AggregatedEventHandler(Action<T> action)
        {
            Action = action;
        }

        public Action<T> Action { get; private set; }

        public override void InvokeCode(object parameter)
        {
            Action((T)parameter);
        }
    }
}

