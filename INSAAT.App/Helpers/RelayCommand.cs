﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace INSAAT.App
{
    public class RelayCommand : ICommand
    {
        private readonly Action<object> commandAction;
        private readonly Func<bool> canExecuteFunc;
        private readonly System.Threading.SynchronizationContext syncContext;

        public RelayCommand(Action<object> action, Func<bool> canExecute)
        {
            commandAction = action;
            canExecuteFunc = canExecute;
            syncContext = System.Threading.SynchronizationContext.Current;
        }

        public RelayCommand(Action<object> action)
            : this(action, null)
        {

        }

        public bool CanExecute(object parameter)
        {
            if (canExecuteFunc != null)
                return canExecuteFunc();
            return true;
        }

        public void NotifyCanExecuteChanged()
        {
            if (canExecuteFunc != null && CanExecuteChanged != null)
            {
                syncContext.Post(new System.Threading.SendOrPostCallback((o) =>
                {
                    CanExecuteChanged(o, EventArgs.Empty);
                }), this);
            }
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            commandAction(parameter);
        }
    }
}
