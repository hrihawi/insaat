﻿using iTextSharp.text.pdf;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace INSAAT.App.Helpers
{
    public static class PDFPreviewHelper
    {
        public static byte[] GetImagePreviewFromPDF(byte[] pdfData)
        {
            try
            {
                string pdfFilePath = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
                File.WriteAllBytes(pdfFilePath, pdfData);
                return GetImagePreviewFromPDF(pdfFilePath);
            }
            catch (Exception) { }
            return null;
        }

        public static byte[] GetImagePreviewFromPDF(string pdfFilePath)
        {
            try
            {
                var firstPage = ExtractPage(pdfFilePath, 1);
                string firstPageFilePath = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
                File.WriteAllBytes(firstPageFilePath, firstPage);
                Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();
                doc.LoadFromFile(firstPageFilePath);
                Image emf = doc.SaveAsImage(0, Spire.Pdf.Graphics.PdfImageType.Metafile);
                return resizePortfoiloImage(emf);
            }
            catch (Exception) { }
            return null;
        }

        static byte[] resizePortfoiloImage(Image imageToResize, double scale = 0.3)
        {
            Bitmap newImage = new Bitmap((int)Math.Round(imageToResize.Width * scale), (int)Math.Round(imageToResize.Height * scale));
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(imageToResize, new Rectangle(0, 0, (int)Math.Round(imageToResize.Width * scale), (int)Math.Round(imageToResize.Height * scale)));
            }

            MemoryStream imageOut = new MemoryStream();
            newImage.Save(imageOut, System.Drawing.Imaging.ImageFormat.Jpeg);
            newImage.Dispose();
            return imageOut.ToArray();
        }

        static byte[] ExtractPage(string sourcePdfPath, int pageNumber)
        {
            PdfReader reader = null;
            iTextSharp.text.Document document = null;
            PdfCopy pdfCopyProvider = null;
            PdfImportedPage importedPage = null;

            // Intialize a new PdfReader instance with the contents of the source Pdf file:
            reader = new PdfReader(sourcePdfPath);

            // Capture the correct size and orientation for the page:
            document = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(pageNumber));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            MemoryStream MemoryStream = new MemoryStream();
            pdfCopyProvider = new PdfCopy(document, MemoryStream);

            document.Open();

            // Extract the desired page number:
            importedPage = pdfCopyProvider.GetImportedPage(reader, pageNumber);
            pdfCopyProvider.AddPage(importedPage);
            document.Close();
            reader.Close();
            return MemoryStream.ToArray();
        }
    }
}
