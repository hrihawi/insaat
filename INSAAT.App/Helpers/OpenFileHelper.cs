﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.App.Helpers
{
    public static class OpenFileHelper
    {
        public static string ImagesTypesFilter = "All Images Types|*.bmp;*.jpg;*.jpeg;*.png;";
        public static string DocumentsTypesFilter = "PDF|*.pdf|"
                        + "All Documents Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.txt;*.doc;*.docs";
    }
}
