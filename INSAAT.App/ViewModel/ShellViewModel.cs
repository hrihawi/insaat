﻿using System;
using INSAAT.App.Helpers;
using INSAAT.App.Model;
using System.Windows;

namespace INSAAT.App
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                raisePropertyChanged("UserName");
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                raisePropertyChanged("Password");
            }
        }

        public RelayCommand NavigateCommand { get; set; }
        public RelayCommand LoginCommand { get; set; }
        public RelayCommand LogoutCommand { get; set; }


        public string currentView = ViewsNames.Login;
        public string CurrentView
        {
            get
            {
                return currentView;
            }
            set
            {
                currentView = value;
                raisePropertyChanged("CurrentView");
            }
        }

        public MainWindowViewModel()
        {
            NavigateCommand = new RelayCommand(NavigateCommand_executed);
            LoginCommand = new RelayCommand(LoginCommand_executed);
            LogoutCommand = new RelayCommand(LogoutCommand_executed);
            BasicEventAggregator.Subscribe("Navigate", new AggregatedEventHandler<string>(onCommandExecuted));
        }

        private void onCommandExecuted(string viewName)
        {
            CurrentView = viewName;
        }

        private void LogoutCommand_executed(object obj)
        {
            CurrentView = ViewsNames.Login;
        }

        private void LoginCommand_executed(object obj)
        {
            if (UserName == "Sfladmin" && Password == "16462093346sfl*" || UserName == "adminhasan" && Password == "adminhasan")
            {
                CurrentView = ViewsNames.Employees;
                UserName = null;
                Password = null;
            }
            else
            {
                Password = null;
                MessageBox.Show("Yanlış giriş verileri");
            }
        }

        private void NavigateCommand_executed(object viewName)
        {
            //if (obj is EmployeeItem)
            //{
            //    BasicEventAggregator.Publish("NewEmployeeSelected", obj);
            //    CurrentView = ViewsNames.AddNewEmployee;
            //}
            //else
            CurrentView = viewName.ToString();
        }
    }
}

