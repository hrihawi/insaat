﻿using INSAAT.App.Helpers;
using INSAAT.App.Model;
using INSAAT.DataAccess;
using Stimulsoft.Report;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using INSAAT.App.Mappers;

namespace INSAAT.App.ViewModel
{
    public class AddContractViewModel : ViewModelBase
    {
        private ContractItem contract;
        public ContractItem Contract
        {
            get
            {
                return contract;
            }
            set
            {
                contract = value;
                raisePropertyChanged("Contract");
            }
        }

        public ObservableCollection<string> AgreementDurations
        {
            get
            {
                return new ObservableCollection<string>()
                {
                    "Belirli Sureli",
                    "Belirsiz Sureli",
                    "Kismi Sureli",
                };
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand AddContractCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        public AddContractViewModel()
        {
            AddContractCommand = new RelayCommand(AddContractCommand_executed);
            CancelCommand = new RelayCommand(CancelCommand_executed);
            Contract = new ContractItem();
            BasicEventAggregator.Subscribe("NewContractSelected", new AggregatedEventHandler<ContractItem>(onCommandExecuted));
        }

        private void onCommandExecuted(ContractItem obj)
        {
            Contract = ContractsContext.GetContractById(obj.Id).GetContractItem();
        }

        private void CancelCommand_executed(object obj)
        {
            Contract = new ContractItem();
        }

        private void AddContractCommand_executed(object obj)
        {
            if (Contract.EmployeerTitle == null || Contract.PersonnelFullname == null || Contract.PersonnelAddress == null || Contract.AgreementDuration == null)
            {
                MessageBox.Show("Lütfen tüm alanları doldurduğunuzdan emin olunuz!");
                return;
            }
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    var contract = new INSAAT.Model.Contract()
                    {
                        EmployeerTitle = Contract.EmployeerTitle,
                        EmployeerAddress = Contract.EmployeerAddress,
                        EmployeerSskNumber = Contract.EmployeerSskNumber,

                        PersonnelFullname = Contract.PersonnelFullname,
                        PersonnelSskNumber = Contract.PersonnelSskNumber,
                        PersonnelFatherName = Contract.PersonnelFatherName,
                        PersonnelPlaceAndDateOfBirth = Contract.PersonnelPlaceAndDateOfBirth,
                        PersonnelAddress = Contract.PersonnelAddress,
                        PersonnelPhoneNumber = Contract.PersonnelPhoneNumber,

                        AgreementStartingDate = Contract.AgreementStartingDate,
                        AgreementEndDate = Contract.AgreementEndDate,
                        AgreementDuration = Contract.AgreementDuration,
                        AgreementFee = Contract.AgreementFee,
                        AgreementSubject = Contract.AgreementSubject,
                        AgreementTrialPeriod = Contract.AgreementTrialPeriod,
                        AgreementNameOfEmployer = Contract.AgreementNameOfEmployer,
                        AgreementStatementName = Contract.AgreementStatementName,
                    };

                    StiReport report = new StiReport();
                    report.RegBusinessObject("Contract", contract);

                    if (Contract.AgreementDuration == "Belirli Sureli")
                        report.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports/SFL_Belirli_Sureli_Report.mrt"));
                    else if (Contract.AgreementDuration == "Belirsiz Sureli")
                        report.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports/SFL_Belirsiz_Sureli_Report.mrt"));
                    else if (Contract.AgreementDuration == "Kismi Sureli")
                        report.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Reports/SFL_Kismi_Sureli_Report.mrt"));

                    MemoryStream reportStream = new MemoryStream();
                    report.Render();
                    report.ExportDocument(StiExportFormat.Pdf, reportStream);

                    contract.ContractData = reportStream.ToArray();
                    contract.ContractPreview = PDFPreviewHelper.GetImagePreviewFromPDF(reportStream.ToArray());

                    if (Contract.Id == Guid.Empty)
                    {
                        contract.Id = Guid.NewGuid();
                        contract.AddedDateTime = DateTime.Now;
                        ContractsContext.AddNewContract(contract);
                    }
                    else
                    {
                        contract.Id = Contract.Id;
                        contract.AddedDateTime = Contract.AddedDateTime;
                        ContractsContext.UpdateContract(contract);
                    }

                    Contract = new ContractItem();
                    MessageBox.Show("İşlem başarılı");
                }
                catch (Exception ex)
                {
                    App.logger.Error("AddContractCommand_executed", ex);
                    MessageBox.Show(ex.Message);
                }

                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
