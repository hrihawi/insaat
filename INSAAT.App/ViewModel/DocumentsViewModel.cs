﻿using INSAAT.DataAccess;
using INSAAT.Model;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using INSAAT.App.Mappers;

namespace INSAAT.App.ViewModel
{
    public class DocumentsViewModel : ViewModelBase
    {
        private ObservableCollection<DocumentItem> documents;
        public ObservableCollection<DocumentItem> Documents
        {
            get
            {
                return documents;
            }
            set
            {
                documents = value;
                raisePropertyChanged("Documents");
            }
        }

        private DocumentItem selectedDocument;
        public DocumentItem SelectedDocument
        {
            get
            {
                return selectedDocument;
            }
            set
            {
                selectedDocument = value;
                raisePropertyChanged("SelectedDocument");
            }
        }

        private string searchKeyword;
        public string SearchKeyword
        {
            get
            {
                return searchKeyword;
            }
            set
            {
                searchKeyword = value;
                raisePropertyChanged("SearchKeyword");
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand SearchDocumentsCommand { get; set; }

        public RelayCommand OpenCommand { get; set; }

        public RelayCommand DeleteCommand { get; set; }

        public DocumentsViewModel()
        {
            SearchDocumentsCommand = new RelayCommand(SearchDocumentsCommand_Executed);
            OpenCommand = new RelayCommand(OpenDocumentCommand_Executed);
            DeleteCommand = new RelayCommand(DeleteCommand_Executed);
        }

        private void DeleteCommand_Executed(object obj)
        {
            try
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Emin misiniz?", "silme Onayı", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    DocumentsContext.DeleteDocument(SelectedDocument.Id);
                    SearchDocumentsCommand_Executed(null);
                }
            }
            catch (Exception ex)
            {
                App.logger.Error("DeleteCommand_Executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenDocumentCommand_Executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    var document = DocumentsContext.GetDocumentById(((DocumentItem)obj).Id);
                    string filePath = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
                    File.WriteAllBytes(filePath, document.DocumentData);
                    System.Diagnostics.Process.Start(filePath);
                }
                catch (Exception ex)
                {
                    App.logger.Error("OpenDocumentCommand_Executed", ex);
                    MessageBox.Show(ex.Message);
                }
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }

        private void SearchDocumentsCommand_Executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                var list = DocumentsContext.SearchDocuments(SearchKeyword).Select(item => item.GetDocumentItem()).ToList();
                Documents = new ObservableCollection<DocumentItem>(list);
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
