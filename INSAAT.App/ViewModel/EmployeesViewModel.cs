﻿using INSAAT.DataAccess;
using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using INSAAT.App.Mappers;
using INSAAT.App.Helpers;
using INSAAT.App.Model;

namespace INSAAT.App.ViewModel
{
    public class EmployeesViewModel : ViewModelBase
    {
        private ObservableCollection<EmployeeItem> employees;
        public ObservableCollection<EmployeeItem> Employees
        {
            get
            {
                return employees;
            }
            set
            {
                employees = value;
                raisePropertyChanged("Employees");
            }
        }

        private EmployeeItem selectedEmployee;
        public EmployeeItem SelectedEmployee
        {
            get
            {
                return selectedEmployee;
            }
            set
            {
                selectedEmployee = value;
                raisePropertyChanged("SelectedEmployee");
            }
        }

        private string searchKeyword;
        public string SearchKeyword
        {
            get
            {
                return searchKeyword;
            }
            set
            {
                searchKeyword = value;
                raisePropertyChanged("SearchKeyword");
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand SearchEmployeesCommand { get; set; }

        public RelayCommand EditCommand { get; set; }

        public RelayCommand DeleteCommand { get; set; }


        public EmployeesViewModel()
        {
            SearchEmployeesCommand = new RelayCommand(SearchEmployeesCommand_Executed);
            EditCommand = new RelayCommand(EditCommand_Executed);
            DeleteCommand = new RelayCommand(DeleteCommand_Executed);
        }

        private void DeleteCommand_Executed(object obj)
        {
            try
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Emin misiniz?", "silme Onayı", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    EmployeesContext.DeleteEmployee(SelectedEmployee.Id);
                    SearchEmployeesCommand_Executed(null);
                }
            }
            catch (Exception ex)
            {
                App.logger.Error("DeleteCommand_Executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void EditCommand_Executed(object obj)
        {
            BasicEventAggregator.Publish("NewEmployeeSelected", SelectedEmployee);
            BasicEventAggregator.Publish("Navigate", ViewsNames.AddNewEmployee);
        }

        private void SearchEmployeesCommand_Executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                var list = EmployeesContext.SearchEmployees(SearchKeyword).Select(item => item.GetEmployeeItem()).ToList();
                Employees = new ObservableCollection<EmployeeItem>(list);
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
