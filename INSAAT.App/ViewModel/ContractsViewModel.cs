﻿using INSAAT.App.Model;
using INSAAT.DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INSAAT.App.Mappers;
using System.IO;
using System.Windows;
using INSAAT.App.Helpers;

namespace INSAAT.App.ViewModel
{
    public class ContractsViewModel : ViewModelBase
    {
        private ObservableCollection<ContractItem> contracts;
        public ObservableCollection<ContractItem> Contracts
        {
            get
            {
                return contracts;
            }
            set
            {
                contracts = value;
                raisePropertyChanged("Contracts");
            }
        }

        private ContractItem selectedContract;
        public ContractItem SelectedContract
        {
            get
            {
                return selectedContract;
            }
            set
            {
                selectedContract = value;
                raisePropertyChanged("SelectedContract");
            }
        }

        private string searchKeyword;
        public string SearchKeyword
        {
            get
            {
                return searchKeyword;
            }
            set
            {
                searchKeyword = value;
                raisePropertyChanged("SearchKeyword");
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand SearchContractsCommand { get; set; }

        public RelayCommand OpenCommand { get; set; }

        public RelayCommand EditCommand { get; set; }

        public RelayCommand DeleteCommand { get; set; }



        public ContractsViewModel()
        {
            SearchContractsCommand = new RelayCommand(SearchContractsCommand_Executed);
            OpenCommand = new RelayCommand(OpenContractCommand_Executed);
            EditCommand = new RelayCommand(EditCommand_Executed);
            DeleteCommand = new RelayCommand(DeleteCommand_Executed);
        }

        private void DeleteCommand_Executed(object obj)
        {
            try
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Emin misiniz?", "silme Onayı", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    ContractsContext.DeleteContract(SelectedContract.Id);
                    SearchContractsCommand_Executed(null);
                }
            }
            catch (Exception ex)
            {
                App.logger.Error("DeleteCommand_Executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void EditCommand_Executed(object obj)
        {
            BasicEventAggregator.Publish("NewContractSelected", SelectedContract);
            BasicEventAggregator.Publish("Navigate", ViewsNames.AddContract);
        }

        private void OpenContractCommand_Executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    var contract = ContractsContext.GetContractById(((ContractItem)obj).Id);
                    string filePath = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
                    File.WriteAllBytes(filePath, contract.ContractData);
                    System.Diagnostics.Process.Start(filePath);
                }
                catch (Exception ex)
                {
                    App.logger.Error("OpenContractCommand_Executed", ex);
                    MessageBox.Show(ex.Message);
                }
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }

        private void SearchContractsCommand_Executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                var list = ContractsContext.SearchContracts(SearchKeyword).Select(item => item.GetContractItem()).ToList();
                Contracts = new ObservableCollection<ContractItem>(list);
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
