﻿using INSAAT.App.Converters;
using INSAAT.App.Helpers;
using INSAAT.DataAccess;
using INSAAT.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace INSAAT.App.ViewModel
{
    public class AddNewEmployeeViewModel : ViewModelBase
    {
        private EmployeeItem employee;
        public EmployeeItem Employee
        {
            get
            {
                return employee;
            }
            set
            {
                employee = value;
                raisePropertyChanged("Employee");
            }
        }

        private string cvPath;
        public string CVPath
        {
            get
            {
                return cvPath;
            }
            set
            {
                cvPath = value;
                raisePropertyChanged("CVPath");
            }
        }

        public ObservableCollection<string> Degrees
        {
            get
            {
                return new ObservableCollection<string>()
                {
                    "Lise derecesi",
                    "üniversite derecesi"
                };
            }
        }

        public ObservableCollection<string> Departments
        {
            get
            {
                return new ObservableCollection<string>()
                {
                    "Bölüm 1",
                    "Bölüm 2"
                };
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand AddEmployeeCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public RelayCommand UploadImageCommand { get; set; }

        public RelayCommand UploadCVCommand { get; set; }

        public RelayCommand OpenCvCommand { get; set; }

        public AddNewEmployeeViewModel()
        {
            Employee = new EmployeeItem();
            AddEmployeeCommand = new RelayCommand(AddEmployeeCommand_executed);
            CancelCommand = new RelayCommand(CancelCommand_executed);
            UploadImageCommand = new RelayCommand(UploadImageCommand_executed);
            UploadCVCommand = new RelayCommand(UploadCVCommand_executed);
            OpenCvCommand = new RelayCommand(OpenCvCommand_executed);
            BasicEventAggregator.Subscribe("NewEmployeeSelected", new AggregatedEventHandler<EmployeeItem>(onCommandExecuted));
        }

        private void onCommandExecuted(EmployeeItem obj)
        {
            Employee = obj;
        }

        private void OpenCvCommand_executed(object obj)
        {
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    string filePath = string.Format("{0}{1}.pdf", Path.GetTempPath(), Guid.NewGuid().ToString());
                    File.WriteAllBytes(filePath, Employee.CV);
                    System.Diagnostics.Process.Start(filePath);
                }
                catch (Exception ex)
                {
                    App.logger.Error("OpenCvCommand_executed", ex);
                    MessageBox.Show(ex.Message);
                }
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }

        private void CancelCommand_executed(object obj)
        {
            Employee = new EmployeeItem();
            CVPath = null;
        }

        private void UploadCVCommand_executed(object obj)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = OpenFileHelper.DocumentsTypesFilter;
                if (openFileDialog.ShowDialog() == true)
                {
                    CVPath = openFileDialog.FileName;
                    Employee.CV = File.ReadAllBytes(openFileDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                App.logger.Error("UploadCVCommand_executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void UploadImageCommand_executed(object obj)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = OpenFileHelper.ImagesTypesFilter;
                if (openFileDialog.ShowDialog() == true)
                    Employee.Image = File.ReadAllBytes(openFileDialog.FileName);
            }
            catch (Exception ex)
            {
                App.logger.Error("OpenFileCommand_executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void AddEmployeeCommand_executed(object obj)
        {
            if (employee.FirstName == null || employee.LastName == null || employee.PhoneNumber == null || employee.TCNumber == null)
            {
                MessageBox.Show("Lütfen tüm alanları doldurduğunuzdan emin olunuz!");
                return;
            }
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    if (Employee.Id == Guid.Empty)
                    {
                        EmployeesContext.AddNewEmployee(new Employee()
                        {
                            Id = Guid.NewGuid(),
                            AddedDateTime = DateTime.Now,
                            Address = employee.Address,
                            CV = employee.CV,
                            Degree = employee.Degree,
                            Department = employee.Department,
                            FirstName = employee.FirstName,
                            Image = employee.Image,
                            LastName = employee.LastName,
                            PhoneNumber = employee.PhoneNumber,
                            TCNumber = employee.TCNumber,
                        });
                    }
                    else
                    {
                        EmployeesContext.UpdateEmployee(new Employee()
                        {
                            Id = Employee.Id,
                            AddedDateTime = Employee.AddedDateTime,
                            Address = Employee.Address,
                            CV = Employee.CV,
                            Degree = Employee.Degree,
                            Department = Employee.Department,
                            FirstName = Employee.FirstName,
                            Image = Employee.Image,
                            LastName = Employee.LastName,
                            PhoneNumber = Employee.PhoneNumber,
                            TCNumber = Employee.TCNumber,
                        });
                    }

                    Employee = new EmployeeItem();
                    CVPath = null;
                    MessageBox.Show("İşlem başarılı");
                }
                catch (Exception ex)
                {
                    App.logger.Error("AddEmployeeCommand_executed", ex);
                    MessageBox.Show("bir hata oluştu");
                }
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
