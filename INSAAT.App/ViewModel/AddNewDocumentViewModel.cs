﻿using INSAAT.App.Helpers;
using INSAAT.DataAccess;
using INSAAT.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace INSAAT.App.ViewModel
{
    public class AddNewDocumentViewModel : ViewModelBase
    {
        private DocumentItem document;
        public DocumentItem Document
        {
            get
            {
                return document;
            }
            set
            {
                document = value;
                raisePropertyChanged("Document");
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                isBusy = value;
                raisePropertyChanged("IsBusy");
            }
        }

        public RelayCommand AddDocumentCommand { get; set; }

        public RelayCommand CancelCommand { get; set; }

        public RelayCommand OpenFileCommand { get; set; }

        public AddNewDocumentViewModel()
        {
            AddDocumentCommand = new RelayCommand(AddDocumentCommand_executed);
            CancelCommand = new RelayCommand(CancelCommand_executed);
            OpenFileCommand = new RelayCommand(OpenFileCommand_executed);
            Document = new DocumentItem();
        }

        private void CancelCommand_executed(object obj)
        {
            Document = new DocumentItem();
        }

        private void OpenFileCommand_executed(object obj)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = OpenFileHelper.DocumentsTypesFilter;
                if (openFileDialog.ShowDialog() == true)
                {
                    Document.Name = Path.GetFileName(openFileDialog.FileName);
                    Document.DocumentPath = openFileDialog.FileName;
                    Document.DocumentData = File.ReadAllBytes(openFileDialog.FileName);
                    Document.DocumentPreview = PDFPreviewHelper.GetImagePreviewFromPDF(openFileDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                App.logger.Error("OpenFileCommand_executed", ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void AddDocumentCommand_executed(object obj)
        {
            if (Document.DocumentData == null)
            {
                MessageBox.Show("Lütfen tüm alanları doldurduğunuzdan emin olunuz!");
                return;
            }
            IsBusy = true;
            var ts = Task.Factory.StartNew(() =>
            {
                try
                {
                    var document = new Document()
                    {
                        DocumentName = Document.Name,
                        DocumentData = Document.DocumentData,
                        DocumentPreview = Document.DocumentPreview,
                        AddedDateTime = DateTime.Now,
                    };
                    DocumentsContext.AddNewDocument(document);
                    Document = new DocumentItem();
                    MessageBox.Show("İşlem başarılı");
                }
                catch (Exception ex)
                {
                    App.logger.Error("AddDocumentCommand_executed", ex);
                    MessageBox.Show("bir hata oluştu");
                }
                Application.Current.Dispatcher.Invoke(new Action(() => IsBusy = false));
            });
        }
    }
}
