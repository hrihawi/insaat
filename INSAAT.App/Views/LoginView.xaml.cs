﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace INSAAT.App.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        private Storyboard initialAnimationBoard;
        public LoginView()
        {
            InitializeComponent();
            StartInitialAnimation();
        }

        public void StartInitialAnimation()
        {
            initialAnimationBoard = new Storyboard();
            //fingerPrint.Fill = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffffff"));
            var animHeight = new DoubleAnimation(Logo.Height, Logo.Height + 15, TimeSpan.FromSeconds(1));
            var animWidth = new DoubleAnimation(Logo.Width, Logo.Width + 15, TimeSpan.FromSeconds(1));

            initialAnimationBoard.Children.Add(animHeight);
            initialAnimationBoard.Children.Add(animWidth);

            Storyboard.SetTarget(animHeight, Logo);
            Storyboard.SetTarget(animWidth, Logo);
            Storyboard.SetTargetProperty(animHeight, new PropertyPath("(Height)"));
            Storyboard.SetTargetProperty(animWidth, new PropertyPath("(Width)"));
            initialAnimationBoard.RepeatBehavior = RepeatBehavior.Forever;
            initialAnimationBoard.AutoReverse = true;
            initialAnimationBoard.SpeedRatio = 1.2;

            initialAnimationBoard.Begin();
        }
    }
}
