﻿using INSAAT.App.Model;
using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.App.Mappers
{
    public static class Mappers
    {
        public static ContractItem GetContractItem(this Contract contact)
        {
            return new ContractItem()
            {
                Id = contact.Id,
                ContractData = contact.ContractData,
                ContractPreview = contact.ContractPreview,

                AddedDateTime = contact.AddedDateTime,

                EmployeerTitle = contact.EmployeerTitle,
                EmployeerAddress = contact.EmployeerAddress,
                EmployeerSskNumber = contact.EmployeerSskNumber,

                PersonnelFullname = contact.PersonnelFullname,
                PersonnelSskNumber = contact.PersonnelSskNumber,
                PersonnelFatherName = contact.PersonnelFatherName,
                PersonnelPlaceAndDateOfBirth = contact.PersonnelPlaceAndDateOfBirth,
                PersonnelAddress = contact.PersonnelAddress,
                PersonnelPhoneNumber = contact.PersonnelPhoneNumber,

                AgreementStartingDate = contact.AgreementStartingDate,
                AgreementEndDate = contact.AgreementEndDate,
                AgreementDuration = contact.AgreementDuration,
                AgreementFee = contact.AgreementFee,
                AgreementSubject = contact.AgreementSubject,
                AgreementTrialPeriod = contact.AgreementTrialPeriod,
                AgreementNameOfEmployer = contact.AgreementNameOfEmployer,
                AgreementStatementName = contact.AgreementStatementName,
                
            };
        }

        public static DocumentItem GetDocumentItem(this Document document)
        {
            return new DocumentItem()
            {
                Id= document.Id,
                Name= document.DocumentName,
                DocumentData= document.DocumentData,
                DocumentPreview= document.DocumentPreview,
                AddedDateTime= document.AddedDateTime,
            };
        }

        public static EmployeeItem GetEmployeeItem(this Employee employee)
        {
            return new EmployeeItem()
            {
                Id = employee.Id,
                AddedDateTime = employee.AddedDateTime,
                Address = employee.Address,
                CV = employee.CV,
                Degree = employee.Degree,
                Department = employee.Department,
                FirstName = employee.FirstName,
                Image = employee.Image,
                LastName = employee.LastName,
                PhoneNumber = employee.PhoneNumber,
                TCNumber = employee.TCNumber,
            };
        }
    }
}
