﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.App.Model
{
    public static class ViewsNames
    {
        public const string Login = "Login";

        public const string AddContract = "AddContract";
        public const string AddNewDocument = "AddNewDocument";
        public const string AddNewEmployee = "AddNewEmployee";
        public const string Contracts = "Contracts";
        public const string Documents = "Documents";
        public const string Employees = "Employees";
    }
}
