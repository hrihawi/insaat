﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.App.Model
{
    public class ContractItem : ViewModelBase
    {
        private Guid id;
        public Guid Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                raisePropertyChanged("Id");
            }
        }

        private string contractPath;
        public string ContractPath
        {
            get
            {
                return contractPath;
            }
            set
            {
                contractPath = value;
                raisePropertyChanged("ContractPath");
            }
        }

        private byte[] contractData;
        public byte[] ContractData
        {
            get
            {
                return contractData;
            }
            set
            {
                contractData = value;
                raisePropertyChanged("ContractData");
            }
        }

        private byte[] contractPreview;
        public byte[] ContractPreview
        {
            get
            {
                return contractPreview;
            }
            set
            {
                contractPreview = value;
                raisePropertyChanged("ContractPreview");
            }
        }



        private DateTime addedDateTime = DateTime.Now;
        public DateTime AddedDateTime
        {
            get
            {
                return addedDateTime;
            }
            set
            {
                addedDateTime = value;
                raisePropertyChanged("AddedDateTime");
            }
        }


        // ÜNVANI			
        private string employeerTitle;
        public string EmployeerTitle
        {
            get
            {
                return employeerTitle;
            }
            set
            {
                employeerTitle = value;
                raisePropertyChanged("EmployeerTitle");
            }
        }

        // ADRESİ			
        private string employeerAddress;
        public string EmployeerAddress
        {
            get
            {
                return employeerAddress;
            }
            set
            {
                employeerAddress = value;
                raisePropertyChanged("EmployeerAddress");
            }
        }

        // SSK İş yeri Sicil No:
        private string employeerSskNumber;
        public string EmployeerSskNumber
        {
            get
            {
                return employeerSskNumber;
            }
            set
            {
                employeerSskNumber = value;
                raisePropertyChanged("EmployeerSskNumber");
            }
        }


        // Adı ve Soyadı	
        private string personnelFullname;
        public string PersonnelFullname
        {
            get
            {
                return personnelFullname;
            }
            set
            {
                personnelFullname = value;
                raisePropertyChanged("PersonnelFullname");
            }
        }

        // SSK Sicil No
        private string personnelSskNumber;
        public string PersonnelSskNumber
        {
            get
            {
                return personnelSskNumber;
            }
            set
            {
                personnelSskNumber = value;
                raisePropertyChanged("PersonnelSskNumber");
            }
        }

        // Baba Adı
        private string personnelFatherName;
        public string PersonnelFatherName
        {
            get
            {
                return personnelFatherName;
            }
            set
            {
                personnelFatherName = value;
                raisePropertyChanged("PersonnelFatherName");
            }
        }

        // Doğum Yeri ve Yılı	
        private string personnelPlaceAndDateOfBirth;
        public string PersonnelPlaceAndDateOfBirth
        {
            get
            {
                return personnelPlaceAndDateOfBirth;
            }
            set
            {
                personnelPlaceAndDateOfBirth = value;
                raisePropertyChanged("PersonnelPlaceAndDateOfBirth");
            }
        }

        // İkametgah Adresi		:
        private string personnelAddress;
        public string PersonnelAddress
        {
            get
            {
                return personnelAddress;
            }
            set
            {
                personnelAddress = value;
                raisePropertyChanged("PersonnelAddress");
            }
        }

        // Ev ve Cep Telefon No:		:
        private string personnelPhoneNumber;
        public string PersonnelPhoneNumber
        {
            get
            {
                return personnelPhoneNumber;
            }
            set
            {
                personnelPhoneNumber = value;
                raisePropertyChanged("PersonnelPhoneNumber");
            }
        }



        // Başlangıç Tarihi
        private DateTime agreementStartingDate = DateTime.Now;
        public DateTime AgreementStartingDate
        {
            get
            {
                return agreementStartingDate;
            }
            set
            {
                agreementStartingDate = value;
                raisePropertyChanged("AgreementStartingDate");
            }
        }

        // Bitiş Tarihi
        private DateTime agreementEndDate = DateTime.Now.AddDays(30);
        public DateTime AgreementEndDate
        {
            get
            {
                return agreementEndDate;
            }
            set
            {
                agreementEndDate = value;
                raisePropertyChanged("AgreementEndDate");
            }
        }

        // Sözleşmenin Süresi (Belirli Sürelidir. ||  Belirsiz_Sureli  ||  Kismi_Sureli)
        private string agreementDuration = "Belirli Sureli";
        public string AgreementDuration
        {
            get
            {
                return agreementDuration;
            }
            set
            {
                agreementDuration = value;
                raisePropertyChanged("AgreementDuration");
            }
        }

        // Ücret
        private string agreementFee;
        public string AgreementFee
        {
            get
            {
                return agreementFee;
            }
            set
            {
                agreementFee = value;
                raisePropertyChanged("AgreementFee");
            }
        }

        // Yapılan Işin Konusu
        private string agreementSubject;
        public string AgreementSubject
        {
            get
            {
                return agreementSubject;
            }
            set
            {
                agreementSubject = value;
                raisePropertyChanged("AgreementSubject");
            }
        }

        // Deneme Süresi
        private string agreementTrialPeriod;
        public string AgreementTrialPeriod
        {
            get
            {
                return agreementTrialPeriod;
            }
            set
            {
                agreementTrialPeriod = value;
                raisePropertyChanged("AgreementTrialPeriod");
            }
        }

        // Sözleşmede adı gecen işveren deyimi
        private string agreementNameOfEmployer;
        public string AgreementNameOfEmployer
        {
            get
            {
                return agreementNameOfEmployer;
            }
            set
            {
                agreementNameOfEmployer = value;
                raisePropertyChanged("AgreementNameOfEmployer");
            }
        }

        // Personel deyimi ise bu sözleşmede ismi gecen
        private string agreementStatementName;
        public string AgreementStatementName
        {
            get
            {
                return agreementStatementName;
            }
            set
            {
                agreementStatementName = value;
                raisePropertyChanged("AgreementStatementName");
            }
        }
    }
}
