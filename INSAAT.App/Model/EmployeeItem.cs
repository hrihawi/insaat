﻿using INSAAT.App;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.Model
{
    public class EmployeeItem : ViewModelBase
    {
        private Guid id;
        public Guid Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                raisePropertyChanged("Id");
            }
        }

        private byte[] image;
        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                raisePropertyChanged("Image");
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                raisePropertyChanged("FirstName");
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                raisePropertyChanged("LastName");
            }
        }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        private string tcNumber;
        public string TCNumber
        {
            get
            {
                return tcNumber;
            }
            set
            {
                tcNumber = value;
                raisePropertyChanged("TCNumber");
            }
        }

        private string degree = "Lise derecesi";
        public string Degree
        {
            get
            {
                return degree;
            }
            set
            {
                degree = value;
                raisePropertyChanged("Degree");
            }
        }


        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                raisePropertyChanged("Address");
            }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
                raisePropertyChanged("PhoneNumber");
            }
        }

        private string department = "Bölüm 1";
        public string Department
        {
            get
            {
                return department;
            }
            set
            {
                department = value;
                raisePropertyChanged("Department");
            }
        }

        private byte[] cv;
        public byte[] CV
        {
            get
            {
                return cv;
            }
            set
            {
                cv = value;
                raisePropertyChanged("CV");
            }
        }

        private DateTime addedDateTime;
        public DateTime AddedDateTime
        {
            get
            {
                return addedDateTime;
            }
            set
            {
                addedDateTime = value;
                raisePropertyChanged("AddedDateTime");
            }
        }

    }
}
