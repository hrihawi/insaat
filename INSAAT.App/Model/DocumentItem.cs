﻿using INSAAT.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.Model
{
    public class DocumentItem : ViewModelBase
    {
        private Guid id;
        public Guid Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                raisePropertyChanged("Id");
            }
        }

        private string documentName;
        public string Name
        {
            get
            {
                return documentName;
            }
            set
            {
                documentName = value;
                raisePropertyChanged("Name");
            }
        }

        private byte[] documentData;
        public byte[] DocumentData
        {
            get
            {
                return documentData;
            }
            set
            {
                documentData = value;
                raisePropertyChanged("DocumentData");
            }
        }

        private DateTime addedDateTime;
        public DateTime AddedDateTime
        {
            get
            {
                return addedDateTime;
            }
            set
            {
                addedDateTime = value;
                raisePropertyChanged("AddedDateTime");
            }
        }


        private string documentPath;
        public string DocumentPath
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
                raisePropertyChanged("DocumentPath");
            }
        }

        private byte[] documentPreview;
        public byte[] DocumentPreview
        {
            get
            {
                return documentPreview;
            }
            set
            {
                documentPreview = value;
                raisePropertyChanged("DocumentPreview");
            }
        }
    }
}
