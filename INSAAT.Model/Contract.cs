﻿using System;
using System.ComponentModel.DataAnnotations;

namespace INSAAT.Model
{
    public class Contract
    {
        public Contract()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }

        public byte[] ContractPreview { get; set; }

        public byte[] ContractData { get; set; }

        public DateTime AddedDateTime { get; set; }


        // ÜNVANI			
        public string EmployeerTitle { get; set; }

        // ADRESİ			
        public string EmployeerAddress { get; set; }

        // SSK İş yeri Sicil No:
        public string EmployeerSskNumber { get; set; }



        // Adı ve Soyadı	
        public string PersonnelFullname { get; set; }

        // SSK Sicil No
        public string PersonnelSskNumber { get; set; }

        // Baba Adı
        public string PersonnelFatherName { get; set; }

        // Doğum Yeri ve Yılı	
        public string PersonnelPlaceAndDateOfBirth { get; set; }

        // İkametgah Adresi		:
        public string PersonnelAddress { get; set; }

        // Ev ve Cep Telefon No:		:
        public string PersonnelPhoneNumber { get; set; }



        // Başlangıç Tarihi
        public DateTime AgreementStartingDate { get; set; }

        // Bitiş Tarihi
        public DateTime AgreementEndDate { get; set; }

        // Sözleşmenin Süresi (Belirli Sürelidir. ||  Belirsiz_Sureli  ||  Kismi_Sureli)
        public string AgreementDuration { get; set; }

        // Ücret
        public string AgreementFee { get; set; }

        // Yapılan Işin Konusu
        public string AgreementSubject { get; set; }

        // Deneme Süresi
        public string AgreementTrialPeriod { get; set; }

        // Sözleşmede adı gecen işveren deyimi
        public string AgreementNameOfEmployer { get; set; }

        // Personel deyimi ise bu sözleşmede ismi gecen
        public string AgreementStatementName { get; set; }

    }
}
