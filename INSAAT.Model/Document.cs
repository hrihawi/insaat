﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.Model
{
    public class Document
    {
        public Document()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }

        public string DocumentName { get; set; }

        public DateTime AddedDateTime { get; set; }

        public byte[] DocumentData { get; set; }

        public byte[] DocumentPreview { get; set; }
    }
}
