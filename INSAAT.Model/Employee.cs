﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.Model
{
    public class Employee
    {
        public Employee()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }

        public byte[] Image { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string TCNumber { get; set; }

        public string Degree { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Department { get; set; }

        public byte[] CV { get; set; }

        public DateTime AddedDateTime { get; set; }

    }
}
