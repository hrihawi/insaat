﻿using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace INSAAT.DataAccess
{
    public static class ContractsContext
    {
        public static void AddNewContract(Contract contract)
        {
            using (var context = new InsaatContext())
            {
                context.Contracts.Add(contract);
                context.SaveChanges();
            }
        }

        public static Contract GetContractById(Guid id)
        {
            using (var context = new InsaatContext())
            {
                return context.Contracts.FirstOrDefault(it => it.Id == id);
            }
        }

        public static List<Contract> SearchContracts(string searchKey)
        {
            using (var context = new InsaatContext())
            {
                if (string.IsNullOrWhiteSpace(searchKey))
                {
                    return context.Contracts.Select(x => new
                    {
                        Id = x.Id,
                        ContractPreview = x.ContractPreview,
                        PersonnelFullname = x.PersonnelFullname,
                        EmployeerSskNumber = x.EmployeerSskNumber,
                        PersonnelSskNumber = x.PersonnelSskNumber,
                        AddedDateTime = x.AddedDateTime,

                    })
                    .ToList().Select(x => new Contract()
                    {
                        Id = x.Id,
                        ContractPreview = x.ContractPreview,
                        PersonnelFullname = x.PersonnelFullname,
                        EmployeerSskNumber = x.EmployeerSskNumber,
                        PersonnelSskNumber = x.PersonnelSskNumber,
                        AddedDateTime = x.AddedDateTime,
                    }).OrderByDescending(c => c.AddedDateTime).ToList();
                }
                else
                {
                    return context.Contracts.Where(e =>
                       e.PersonnelFullname.Contains(searchKey.Trim())
                    || e.EmployeerSskNumber.Contains(searchKey.Trim())
                    || e.PersonnelSskNumber.Contains(searchKey.Trim())

                    || e.EmployeerTitle.Contains(searchKey.Trim())
                    || e.EmployeerAddress.Contains(searchKey.Trim())
                    || e.PersonnelAddress.Contains(searchKey.Trim())
                    || e.AgreementDuration.Contains(searchKey.Trim())
                    || e.AgreementSubject.Contains(searchKey.Trim())
                    || e.AgreementNameOfEmployer.Contains(searchKey.Trim())
                    || e.AgreementStatementName.Contains(searchKey.Trim())
                    ).Select(x => new
                    {
                        Id = x.Id,
                        ContractPreview = x.ContractPreview,
                        PersonnelFullname = x.PersonnelFullname,
                        EmployeerSskNumber = x.EmployeerSskNumber,
                        PersonnelSskNumber = x.PersonnelSskNumber,
                        AddedDateTime = x.AddedDateTime,
                    })
                    .ToList().Select(x => new Contract()
                    {
                        Id = x.Id,
                        ContractPreview = x.ContractPreview,
                        PersonnelFullname = x.PersonnelFullname,
                        EmployeerSskNumber = x.EmployeerSskNumber,
                        PersonnelSskNumber = x.PersonnelSskNumber,
                        AddedDateTime = x.AddedDateTime,
                    }).OrderByDescending(c => c.AddedDateTime).ToList();
                }

            }
        }

        public static void DeleteContract(Guid id)
        {
            using (var context = new InsaatContext())
            {
                var oldCont = context.Contracts.FirstOrDefault(x => x.Id == id);
                context.Contracts.Remove(oldCont);
                context.SaveChanges();
            }
        }

        public static void UpdateContract(Contract contract)
        {
            using (var context = new InsaatContext())
            {
                var oldCont = context.Contracts.FirstOrDefault(x => x.Id == contract.Id);
                context.Contracts.Remove(oldCont);
                context.Contracts.Add(contract);
                context.SaveChanges();
            }
        }
    }
}
