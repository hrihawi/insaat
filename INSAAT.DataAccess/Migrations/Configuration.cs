namespace INSAAT.DataAccess.Migrations
{
    using System.Configuration;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<InsaatContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            if (ConfigurationManager.AppSettings["AutomaticMigrationDataLossAllowed"] == "true")
                AutomaticMigrationDataLossAllowed = true;
            ContextKey = "INSAAT.DataAccess.InsaatContext";
        }

        // enable-migrations -EnableAutomaticMigrations -Force

        protected override void Seed(InsaatContext context)
        {
            //  This method will be called after migrating to the latest version.
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
        }
    }
}
