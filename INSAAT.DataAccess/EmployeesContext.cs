﻿using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.DataAccess
{
    public static class EmployeesContext
    {
        public static void AddNewEmployee(Employee employee)
        {
            using (var context = new InsaatContext())
            {
                context.Employees.Add(employee);
                context.SaveChanges();
            }
        }

        public static List<Employee> GetEmployees()
        {
            using (var context = new InsaatContext())
            {
                return context.Employees.ToList();
            }
        }

        public static Employee GetEmployeeById(Guid id)
        {
            using (var context = new InsaatContext())
            {
                return context.Employees.FirstOrDefault(it => it.Id == id);
            }
        }

        public static List<Employee> SearchEmployees(string searchKey)
        {
            using (var context = new InsaatContext())
            {
                if (string.IsNullOrWhiteSpace(searchKey))
                {
                    return context.Employees.OrderByDescending(c => c.AddedDateTime).ToList();
                }
                else
                {
                    var searchKeyT = searchKey.Trim();
                    return context.Employees.Where(
                           e => e.FirstName.Contains(searchKeyT)
                        || e.LastName.Contains(searchKeyT)
                        || e.PhoneNumber.Contains(searchKeyT)
                        || e.TCNumber.Contains(searchKeyT)
                        || e.Address.Contains(searchKeyT)
                        || e.Department.Contains(searchKeyT)
                        || ((e.FirstName ?? "") + " " + (e.LastName ?? "")).Contains(searchKey))

                        .OrderByDescending(c => c.AddedDateTime).ToList();
                }
            }
        }

        public static void UpdateEmployee(Employee employee)
        {
            using (var context = new InsaatContext())
            {
                var oldEmp = context.Employees.FirstOrDefault(x => x.Id == employee.Id);
                context.Employees.Remove(oldEmp);
                context.Employees.Add(employee);
                context.SaveChanges();
            }
        }

        public static void DeleteEmployee(Guid id)
        {
            using (var context = new InsaatContext())
            {
                var oldEmp = context.Employees.FirstOrDefault(x => x.Id == id);
                context.Employees.Remove(oldEmp);
                context.SaveChanges();
            }
        }
    }
}
