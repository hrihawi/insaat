﻿using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.DataAccess
{
    public static class DocumentsContext
    {
        public static void AddNewDocument(Document document)
        {
            using (var context = new InsaatContext())
            {
                context.Documents.Add(document);
                context.SaveChanges();
            }
        }

        public static List<Document> GetDocuments()
        {
            using (var context = new InsaatContext())
            {
                return context.Documents.ToList();
            }
        }

        public static List<Document> SearchDocuments(string searchKey)
        {
            using (var context = new InsaatContext())
            {
                if (string.IsNullOrWhiteSpace(searchKey))
                {
                    return context.Documents.Select(p => new
                    {
                        Id = p.Id,
                        DocumentName = p.DocumentName,
                        DocumentPreview = p.DocumentPreview,
                        AddedDateTime = p.AddedDateTime,

                    })
                    .ToList().Select(x => new Document()
                    {
                        Id = x.Id,
                        DocumentName = x.DocumentName,
                        DocumentPreview = x.DocumentPreview,
                        AddedDateTime = x.AddedDateTime,
                    }).OrderByDescending(c => c.AddedDateTime).ToList();
                }
                else
                {
                    return context.Documents.Where(e => 
                    e.DocumentName.Contains(searchKey.Trim())).Select(p => new
                    {
                        Id = p.Id,
                        DocumentName = p.DocumentName,
                        DocumentPreview = p.DocumentPreview,
                        AddedDateTime = p.AddedDateTime,
                    })
                    .ToList().Select(x => new Document()
                    {
                        Id = x.Id,
                        DocumentName = x.DocumentName,
                        DocumentPreview = x.DocumentPreview,
                        AddedDateTime = x.AddedDateTime,
                    }).OrderByDescending(c => c.AddedDateTime).ToList();
                }
            }
        }

        public static void DeleteDocument(Guid id)
        {
            using (var context = new InsaatContext())
            {
                var oldDoc = context.Documents.FirstOrDefault(x => x.Id == id);
                context.Documents.Remove(oldDoc);
                context.SaveChanges();
            }
        }

        public static Document GetDocumentById(Guid id)
        {
            using (var context = new InsaatContext())
            {
                return context.Documents.FirstOrDefault(d => d.Id == id);
            }
        }
    }
}
