﻿using INSAAT.DataAccess.Migrations;
using INSAAT.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSAAT.DataAccess
{
    internal class InsaatContext : DbContext
    {
        public InsaatContext()
            : base("InsaatConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<InsaatContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Contract> Contracts { get; set; }

    }
}
