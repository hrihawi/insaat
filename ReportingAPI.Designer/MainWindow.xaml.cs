﻿using INSAAT.Model;
using Stimulsoft.Report;
using System.Windows;

namespace ReportingAPI.Designer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SFL_Belirli_Sureli_Click(object sender, RoutedEventArgs e)
        {
            StiReport report = new StiReport();
            report.RegBusinessObject("Contract", new Contract());
            report.Load(@"SFL_Belirli_Sureli_Report.mrt");
            report.DesignWithWpf();
        }

        private void SFL_Belirsiz_Sureli_Click(object sender, RoutedEventArgs e)
        {
            StiReport report = new StiReport();
            report.RegBusinessObject("Contract", new Contract());
            report.Load(@"SFL_Belirsiz_Sureli_Report.mrt");
            report.DesignWithWpf();
        }

        private void SFL_Kismi_Sureli_Click(object sender, RoutedEventArgs e)
        {
            StiReport report = new StiReport();
            report.RegBusinessObject("Contract", new Contract());
            report.Load(@"SFL_Kismi_Sureli_Report.mrt");
            report.DesignWithWpf();
        }
    }
}
